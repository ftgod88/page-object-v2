﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Opera;
using PageObjectV2.ConfigFiles;
using System;

namespace PageObjectV2.Drivers
{
    public static class BrowserManager
    {
        public static IWebDriver driver;
        public static IWebDriver ChooseBrowser()
        {
            string browser = Manager.ConfigurationBrowser();
            switch (browser)
            {
                case "Chrome":
                    return new ChromeDriver();
                case "Firefox":
                    return new FirefoxDriver();
                case "Opera":
                    return new OperaDriver();
                default:
                    throw new Exception("Browser was not created");
            }
        }
        public static IWebDriver CreateDriver()
        {
            return driver = ChooseBrowser();
        }

        public static void QuitFromBrowsers()
        {
            driver.Quit();
        }

        public static void NavigateToUrl()
        {
            driver.Navigate().GoToUrl(Manager.ConfigurationUrl());
        }
    }
}
