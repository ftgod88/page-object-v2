﻿using NUnit.Framework;
using OpenQA.Selenium;
using PageObjectV2.ConfigFiles;
using PageObjectV2.Drivers;

namespace PageObjectV2.Browsers
{
    public class BaseClass
    {
        [SetUp]
        public void Setup()
        {
            BrowserManager.CreateDriver();

            BrowserManager.NavigateToUrl();
        }

        [TearDown]
        public void TearDown()
        {
            BrowserManager.QuitFromBrowsers();
        }
    }
}
