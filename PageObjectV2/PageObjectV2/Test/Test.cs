﻿using NUnit.Framework;
using PageObject.PageObjects;
using PageObjectV2.Browsers;

namespace TestProject1
{

    public class Tests : BaseClass
    {
        public static AuthentificationMenuPageObject AutentificationMenu;

        static Tests() 
        {
            AutentificationMenu = new AuthentificationMenuPageObject();
        }

        [Test]
        public void ExpectedHelpfulMessaheEqualActualHelpfulMessage()
        {
            string expectedMessage = "Please enter your email address to create an account.";

            string actualMessgae =AutentificationMenu.WriteHelpfulMessage();

            Assert.AreEqual(expectedMessage, actualMessgae, "message are not equal");
        }

        [Test]
        public void ExpectedTextOnButtonForgotPasswordEqualActualText()
        {
            string expectedText = "Forgot your password?";

            string actualText = AutentificationMenu.WriteTextFromButtonForgotPasswaord();

            Assert.AreEqual(expectedText, actualText, "Text are not equal");
        }

        [Test]
        public void ExpectedTextOnButtonCreateAnAcсountEqualActualText()
        {
            string expectedText = "Create an account";

            string actualText = AutentificationMenu.WriteTextInButtonCreateAnAccount();

            Assert.AreEqual(expectedText, actualText, "Text are not equal");
        }

        [Test]
        public void SignIn()
        {
            AutentificationMenu.WriteEmailAdressInLogInField("text@gmail.com");

            AutentificationMenu.WritePaswordInLogInForm("pass");

            AutentificationMenu.ClickOnButtonSignUp();

            Assert.IsTrue(AutentificationMenu.IsExistAlertOnPage() ? true : false);
        }
    }
}