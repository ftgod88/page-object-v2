﻿using OpenQA.Selenium;
using PageObjectV2.Browsers;
using PageObjectV2.Drivers;

namespace PageObject.PageObjects
{
    public class AuthentificationMenuPageObject 
    {
        //form registration
        public IWebElement textOverFielWithMailAdress => BrowserManager.driver.FindElement(By.CssSelector("label[for='email_create']"));
        public IWebElement helpfulMessage => BrowserManager.driver.FindElement(By.XPath("//div[contains(@class,'alert alert-danger')]/preceding-sibling::p"));
        public IWebElement mainTextInCreateAnAcoountForm => BrowserManager.driver.FindElement(By.CssSelector("form#login_form h3.page-subheading"));
        public IWebElement buttonCreatAnAcoount => BrowserManager.driver.FindElement(By.CssSelector("button#SubmitCreate"));
        public IWebElement textInButtonCreateAnAccount => BrowserManager.driver.FindElement(By.CssSelector("button#SubmitCreate span"));
        public IWebElement registrationMailAdressField => BrowserManager.driver.FindElement(By.CssSelector("input#email_create"));
        //form log in
        public IWebElement buttonSignUp => BrowserManager.driver.FindElement(By.CssSelector("button#SubmitLogin"));
        public IWebElement forgotPassword => BrowserManager.driver.FindElement(By.CssSelector("p.lost_password.form-group a"));
        public IWebElement passwordField => BrowserManager.driver.FindElement(By.CssSelector("input#passwd"));
        public IWebElement textOverPasswordField => BrowserManager.driver.FindElement(By.CssSelector("label[for='passwd']"));
        public IWebElement logInEmailAdressField => BrowserManager.driver.FindElement(By.CssSelector("input#email"));
        public IWebElement textOverEmailAdressField => BrowserManager.driver.FindElement(By.CssSelector("form#create-account_form h3.page-subheading"));

        //other element
        public IWebElement Alert => BrowserManager.driver.FindElement(By.CssSelector("div.alert.alert-danger p"));

        public void WritePaswordInLogInForm(string password)
        {
            passwordField.SendKeys(password);
        }

        public void WriteEmailAdressInLogInField(string signInEmail)
        {
            logInEmailAdressField.SendKeys(signInEmail);
        }

        public void ClickOnButtonSignUp()
        {
            buttonSignUp.Click();
        }

        public void ClickOnForgotPassword()
        {
            forgotPassword.Click();
        }

        public string WriteTextFromButtonForgotPasswaord()
        {
            return forgotPassword.Text;
        }

        public string WriteMainTextInCreateAnAcoountFormInConsole()
        {
            return mainTextInCreateAnAcoountForm.Text;
        }
        //Registration form method
        public void WriteMailInAdressRegistationForm(string registrtionEmail)
        {
            registrationMailAdressField.SendKeys(registrtionEmail);
        }

        public void ClickOnButtonCreatAnAcoount()
        {
            buttonCreatAnAcoount.Click();
        }

        public string WtriteTextFromCreatAnAccountButton()
        {
            return buttonCreatAnAcoount.Text;
        }

        public string WriteHelpfulMessage()
        {
            return helpfulMessage.Text;
        }
        public string WriteTextInButtonCreateAnAccount()
        {
            return textInButtonCreateAnAccount.Text;
        }

        //other element method
        public bool IsExistAlertOnPage()
        {
            return Alert.Displayed;
        }

    }
}
