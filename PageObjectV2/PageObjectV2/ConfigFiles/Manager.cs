﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace PageObjectV2.ConfigFiles
{
    public static class Manager
    {
        static string path = Path.GetFullPath(@"..//..//..//ConfigFiles//config.json");

        public static string ConfigurationUrl()
        {          
            var Configuration = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile(path)
            .Build();

            var url = Configuration.GetSection("url").Value;

            return url;
        }

        public static string ConfigurationBrowser()
        {
            var Configuration = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile(path)
            .Build();

            var browser = Configuration.GetSection("browser").Value;

            return browser;
        }
    }
}
